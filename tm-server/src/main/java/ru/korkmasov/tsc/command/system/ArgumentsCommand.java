package ru.korkmasov.tsc.command.system;

import ru.korkmasov.tsc.command.AbstractCommand;
import ru.korkmasov.tsc.util.ValidationUtil;

import org.jetbrains.annotations.NotNull;

import java.util.Collection;

public final class ArgumentsCommand extends AbstractCommand {

    @NotNull
    @Override
    public final String arg() {
        return "-a";
    }

    @NotNull
    @Override
    public final String name() {
        return "arguments";
    }

    @NotNull
    @Override
    public final String description() {
        return "Show all arguments";
    }

    @Override
    public void execute() {
        System.out.println("[ARGUMENTS]");
        final Collection<AbstractCommand> arguments = serviceLocator.getCommandService().getArguments();
        for (final AbstractCommand argument : arguments) {
            final String arg = argument.arg();
            if (ValidationUtil.isEmpty(arg)) continue;
            System.out.println(arg);
        }
    }
}

package ru.korkmasov.tsc.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.korkmasov.tsc.model.Session;
import lombok.SneakyThrows;

import java.util.List;

public interface ISessionRepository extends IRepository<Session> {

    boolean contains(@NotNull String id);

    List<Session> findByUserId(@NotNull String userId);

    void removeByUserId(@NotNull String userId);

    @SneakyThrows
    void add(@Nullable Session entity);

    @Nullable
    @SneakyThrows
    Session update(@Nullable Session entity);

}

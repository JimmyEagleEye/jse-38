package ru.korkmasov.tsc.repository;

import ru.korkmasov.tsc.api.repository.IOwnerRepository;
import ru.korkmasov.tsc.model.AbstractOwner;
import ru.korkmasov.tsc.model.Project;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Collection;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public abstract class AbstractOwnerRepository<E extends AbstractOwner> extends AbstractRepository<E> implements IOwnerRepository<E> {

    public AbstractBusinessRepository(@NotNull final Connection connection) {
        super(connection);
    }

    public abstract E add(@NotNull final String userId, @Nullable final E entity);

    @NotNull
    protected final List<E> list = new ArrayList<>();

    @Nullable
    @Override
    public E findById(@NotNull final String userId, @NotNull final String id) throws SQLException {
        @NotNull final String query = "SELECT * FROM " + getTableName() + " where id = ? and user_id=? limit 1";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        statement.setString(2, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        final boolean hasNext = resultSet.next();
        if (!hasNext) return null;
        @NotNull final E result = fetch(resultSet);
        statement.close();
        return result;
    }

    @Override
    public int size(@NotNull String userId) {
        int i = 0;
        for (final E e : list) {
            if (userId.equals(e.getUserId())) i++;
        }
        return i;
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        for (final E e : list) {
            if (id.equals(e.getId()) && userId.equals(e.getUserId())) return true;
        }
        return false;
    }

    public void addAll(@NotNull final String userId, @Nullable final Collection<E> collection) {
        if (collection == null) return;
        for (E item : collection) {
            item.setUserId(userId);
            add(item);
        }
    }

    @SneakyThrows
    public void clear(@NotNull final String userId) {
        @NotNull final String query = "delete from " + getTableName() + " where user_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.executeUpdate();
        statement.close();
    }

    @NotNull
    @SneakyThrows
    public List<E> findAll(@NotNull final String userId) {
        @NotNull final String query = "SELECT * FROM " + getTableName() + " where user_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<E> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    @Nullable
    @SneakyThrows
    public Project removeById(@NotNull final String userId, @Nullable final String id) {
        @NotNull final String query = "delete from " + getTableName() + " where id = ? and user_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        statement.setString(2, userId);
        statement.executeUpdate();
        statement.close();
        return null;
    }

    @SneakyThrows
    public void remove(@NotNull final String userId, @NotNull final E entity) {
        @NotNull final String query = "delete from " + getTableName() + " where id = ? and user_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, entity.getId());
        statement.setString(2, userId);
        statement.executeUpdate();
        statement.close();
    }

}
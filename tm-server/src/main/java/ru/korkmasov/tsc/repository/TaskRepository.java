package ru.korkmasov.tsc.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.korkmasov.tsc.api.repository.ITaskRepository;
import ru.korkmasov.tsc.constant.FieldConst;
import ru.korkmasov.tsc.constant.TableConst;
import ru.korkmasov.tsc.model.Task;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public final class TaskRepository extends AbstractBusinessRepository<Task> implements ITaskRepository {

    public TaskRepository(@NotNull Connection connection) {
        super(connection);
    }

    protected String getTableName() {
        return TableConst.TASK_TABLE;
    }

    @Nullable
    @SneakyThrows
    public List<Task> findAllTaskByProjectId(@NotNull final String userId, @Nullable final String projectId) {
        if (projectId == null) return null;
        @NotNull final String query = "SELECT * FROM " + getTableName() + " where project_id = ? and user_id=?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, projectId);
        statement.setString(2, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<Task> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    @Override
    @SneakyThrows
    public void removeAllTaskByProjectId(@NotNull final String userId, @Nullable final String projectId) {
        @NotNull final String query = "delete from " + getTableName() + " where project_id = ? and user_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(2, projectId);
        statement.setString(1, userId);
        statement.executeUpdate();
        statement.close();
    }

    @Nullable
    public Task bindTaskToProjectById(
            @NotNull final String userId, @Nullable final String taskId, @Nullable final String projectId
    ) {
        if (projectId == null || taskId == null) return null;
        @Nullable final Task task = findById(userId, taskId);
        if (task == null) return null;
        task.setProjectId(projectId);
        update(task);
        return task;
    }

    @Nullable
    public Task unbindTaskById(@NotNull final String userId, @Nullable final String id) {
        if (id == null) return null;
        @Nullable final Task task = findById(userId, id);
        if (task == null) return null;
        task.setProjectId(null);
        update(task);
        return task;
    }

    @SneakyThrows
    protected Task fetch(@Nullable ResultSet row) {
        if (row == null) return null;
        @NotNull final Task project = new Task();
        project.setName(row.getString(FieldConst.NAME));
        project.setDescription(row.getString(FieldConst.DESCRIPTION));
        project.setId(row.getString(FieldConst.ID));
        project.setUserId(row.getString(FieldConst.USER_ID));
        project.setStartDate(row.getDate(FieldConst.START_DATE));
        project.setFinishDate(row.getDate(FieldConst.FINISH_DATE));
        project.setCreated(row.getDate(FieldConst.CREATED));
        project.setProjectId(row.getString(FieldConst.PROJECT_ID));
        return project;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task add(@Nullable final Task entity) {
        if (entity == null) return null;
        @NotNull final String query = "insert into " + getTableName() +
                "(id, name, description, status, start_date, finish_date, created, user_id, project_id)" +
                "values(?,?,?,?,?,?,?,?,?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, entity.getId());
        statement.setString(2, entity.getName());
        statement.setString(3, entity.getDescription());
        statement.setString(4, entity.getStatus().toString());
        statement.setDate(5, prepare(entity.getStartDate()));
        statement.setDate(6, prepare(entity.getFinishDate()));
        statement.setDate(7, prepare(entity.getCreated()));
        statement.setString(8, entity.getUserId());
        statement.setString(9, entity.getProjectId());
        statement.executeUpdate();
        statement.close();
        return entity;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task add(@NotNull final String userId, @Nullable final Task entity) {
        if (entity == null) return null;
        entity.setUserId(userId);
        @NotNull final String query = "insert into " + getTableName() +
                "(id, name, description, status, start_date, finish_date, created, user_id, project_id)" +
                "values(?,?,?,?,?,?,?,?,?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, entity.getId());
        statement.setString(2, entity.getName());
        statement.setString(3, entity.getDescription());
        statement.setString(4, entity.getStatus().toString());
        statement.setDate(5, prepare(entity.getStartDate()));
        statement.setDate(6, prepare(entity.getFinishDate()));
        statement.setDate(7, prepare(entity.getCreated()));
        statement.setString(8, entity.getUserId());
        statement.setString(9, entity.getProjectId());
        statement.executeUpdate();
        statement.close();
        return entity;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task update(@Nullable final Task entity) {
        if (entity == null) return null;
        @NotNull final String query = "update " + getTableName() +
                "set id=?, name=?, description=?, status=?, start_date=?, finish_date=?, created=?, user_id=?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, entity.getId());
        statement.setString(2, entity.getName());
        statement.setString(3, entity.getDescription());
        statement.setString(4, entity.getStatus().toString());
        statement.setDate(5, prepare(entity.getStartDate()));
        statement.setDate(6, prepare(entity.getFinishDate()));
        statement.setDate(7, prepare(entity.getCreated()));
        statement.setString(8, entity.getUserId());
        statement.executeUpdate();
        statement.close();
        return entity;
    }

    @Nullable
    @SneakyThrows
    public Task findByName(@NotNull final String userId, @Nullable final String name) {
        @NotNull final String query = "SELECT * FROM " + getTableName() + " where name = ? and user_id=? limit 1";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, name);
        statement.setString(2, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        final boolean hasNext = resultSet.next();
        if (!hasNext) return null;
        @NotNull final Task result = fetch(resultSet);
        statement.close();
        return result;
    }

    @Nullable
    @SneakyThrows
    public Task findByIndex(@NotNull final String userId, final int index) {
        @NotNull final String query = "SELECT * FROM " + getTableName() + " where user_id=? limit 1 offset ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(2, index - 1);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        final boolean hasNext = resultSet.next();
        if (!hasNext) return null;
        @NotNull final Task result = fetch(resultSet);
        statement.close();
        return result;
    }

    @SneakyThrows
    public void removeByName(@NotNull final String userId, @Nullable final String name) {
        @NotNull final String query = "delete from " + getTableName() + " where name = ? and user_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, name);
        statement.setString(2, userId);
        statement.executeUpdate();
        statement.close();
    }

    @SneakyThrows
    public void removeByIndex(@NotNull final String userId, final int index) {
        @NotNull final String query = "delete from " + getTableName() + " where user_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.executeUpdate();
        statement.close();
    }

    @Nullable
    @SneakyThrows
    public Task add(@NotNull final String userId, @Nullable final String name, @Nullable final String description) {
        if (name == null) return null;
        @NotNull Task project = new Task();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        return add(project);
    }

}
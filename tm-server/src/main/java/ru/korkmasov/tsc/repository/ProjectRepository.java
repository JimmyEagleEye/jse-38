package ru.korkmasov.tsc.repository;

import ru.korkmasov.tsc.api.repository.IProjectRepository;
import ru.korkmasov.tsc.exception.entity.ProjectNotFoundException;
import ru.korkmasov.tsc.model.Project;
import ru.korkmasov.tsc.constant.FieldConst;
import ru.korkmasov.tsc.constant.TableConst;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;


public class ProjectRepository extends AbstractOwnerRepository<Project> implements IProjectRepository {

    public ProjectRepository(@NotNull Connection connection) {
        super(connection);
    }

    protected String getTableName() {
        return TableConst.PROJECT_TABLE;
    }

    @Override
    @SneakyThrows
    protected Project fetch(@Nullable ResultSet row) {
        if (row == null) return null;
        @NotNull final Project project = new Project();
        project.setName(row.getString(FieldConst.NAME));
        project.setDescription(row.getString(FieldConst.DESCRIPTION));
        project.setId(row.getString(FieldConst.ID));
        project.setUserId(row.getString(FieldConst.USER_ID));
        project.setStartDate(row.getDate(FieldConst.START_DATE));
        project.setFinishDate(row.getDate(FieldConst.FINISH_DATE));
        project.setCreated(row.getDate(FieldConst.CREATED));
        return project;
    }

    @Nullable
    public String getIdByName(@NotNull final String userId, @Nullable final String name) {
        for (@NotNull final Project project : list) {
            assert name != null;
            if (name.equals(project.getName())) return project.getId();
        }
        throw new ProjectNotFoundException();
    }

    @Override
    public boolean existsByName(@NotNull final String userId, @NotNull final String name) {
        for (final Project project : list) {
            if (name.equals(project.getName()) && userId.equals(project.getName())) return true;
        }
        return false;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project add(@NotNull final String userId, @Nullable final Project entity) {
        if (entity == null) return null;
        entity.setUserId(userId);
        @NotNull final String query = "insert into " + getTableName() +
                "(id, name, description, status, start_date, finish_date, created, user_id)" +
                "values(?,?,?,?,?,?,?,?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, entity.getId());
        statement.setString(2, entity.getName());
        statement.setString(3, entity.getDescription());
        statement.setString(4, entity.getStatus().toString());
        statement.setDate(5, prepare(entity.getStartDate()));
        statement.setDate(6, prepare(entity.getFinishDate()));
        statement.setDate(7, prepare(entity.getCreated()));
        statement.setString(8, entity.getUserId());
        statement.executeUpdate();
        statement.close();
        return entity;
    }

    @Override
    @SneakyThrows
    public @Nullable Project add(@Nullable final Project entity) {
        if (entity == null) return null;
        @NotNull final String query = "insert into " + getTableName() +
                "(id, name, description, status, start_date, finish_date, created, user_id)" +
                "values(?,?,?,?,?,?,?,?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, entity.getId());
        statement.setString(2, entity.getName());
        statement.setString(3, entity.getDescription());
        statement.setString(4, entity.getStatus().toString());
        statement.setDate(5, prepare(entity.getStartDate()));
        statement.setDate(6, prepare(entity.getFinishDate()));
        statement.setDate(7, prepare(entity.getCreated()));
        statement.setString(8, entity.getUserId());
        statement.executeUpdate();
        statement.close();
        return entity;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project findOneByName(@NotNull final String userId, @Nullable final String name) {
        @NotNull final String query = "SELECT * FROM " + getTableName() + " where name = ? and user_id=? limit 1";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, name);
        statement.setString(2, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        final boolean hasNext = resultSet.next();
        if (!hasNext) return null;
        @NotNull final Project result = fetch(resultSet);
        statement.close();
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project findOneByIndex(@NotNull final String userId, final int index) {
        @NotNull final String query = "SELECT * FROM " + getTableName() + " where user_id=? limit 1 offset ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(2, index - 1);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        final boolean hasNext = resultSet.next();
        if (!hasNext) return null;
        @NotNull final Project result = fetch(resultSet);
        statement.close();
        return result;
    }

    @Override
    @SneakyThrows
    public void removeOneByName(@NotNull final String userId, @Nullable final String name) {
        @NotNull final String query = "delete from " + getTableName() + " where name = ? and user_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, name);
        statement.setString(2, userId);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    @SneakyThrows
    public void removeOneByIndex(@NotNull final String userId, final int index) {
        @NotNull final String query = "delete from " + getTableName() + " where user_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.executeUpdate();
        statement.close();
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project update(@Nullable final Project entity) {
        if (entity == null) return null;
        @NotNull final String query = "update " + getTableName() +
                "set id=?, name=?, description=?, status=?, start_date=?, finish_date=?, created=?, user_id=?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, entity.getId());
        statement.setString(2, entity.getName());
        statement.setString(3, entity.getDescription());
        statement.setString(4, entity.getStatus().toString());
        statement.setDate(5, prepare(entity.getStartDate()));
        statement.setDate(6, prepare(entity.getFinishDate()));
        statement.setDate(7, prepare(entity.getCreated()));
        statement.setString(8, entity.getUserId());
        statement.executeUpdate();
        statement.close();
        return entity;
    }

}

package ru.korkmasov.tsc.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.korkmasov.tsc.marker.DBCategory;
import ru.korkmasov.tsc.model.Project;
import ru.korkmasov.tsc.service.ConnectionService;
import ru.korkmasov.tsc.service.PropertyService;

import java.sql.SQLException;
import java.util.List;

public class ProjectRepositoryTest {

    @Nullable
    private ProjectRepository projectRepository;

    @Nullable
    private Project project;

    @Before
    public void before() {
        ConnectionService connectionService = new ConnectionService(new PropertyService());
        projectRepository = new ProjectRepository(connectionService.getConnection());
        project = projectRepository.add("c9779b3f-1171-4d7d-8dea-1138bbfdc9ce", new Project("Project"));
    }

    @Test
    @Category(DBCategory.class)
    public void add() throws SQLException {
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getId());
        Assert.assertNotNull(project.getName());
        Assert.assertEquals("Project", project.getName());

        @NotNull final Project projectById = projectRepository.findById(project.getId());
        Assert.assertNotNull(projectById);
        Assert.assertEquals(project.getId(), projectById.getId());
    }

    @Test
    @Category(DBCategory.class)
    public void findAll() {
        @NotNull final List<Project> projects = projectRepository.findAll();
        Assert.assertTrue(projects.size() > 1);
    }

    @Test
    @Category(DBCategory.class)
    public void findAllByUserId() {
        @NotNull final List<Project> projects = projectRepository.findAll("c9779b3f-1171-4d7d-8dea-1138bbfdc9ce");
        Assert.assertEquals(1, projects.size());
    }

    @Test
    @Category(DBCategory.class)
    public void findAllByUserIdIncorrect() {
        @NotNull final List<Project> projects = projectRepository.findAll("test");
        Assert.assertNotEquals(1, projects.size());
    }

    @Test
    @Category(DBCategory.class)
    public void findById() throws SQLException {
        @NotNull final Project project = projectRepository.findById("c9779b3f-1171-4d7d-8dea-1138bbfdc9ce", this.project.getId());
        Assert.assertNotNull(project);
    }

    @Test
    @Category(DBCategory.class)
    public void findByIdIncorrect() throws SQLException {
        @NotNull final Project project = projectRepository.findById("c9779b3f-1171-4d7d-8dea-1138bbfdc9ce", "34");
        Assert.assertNull(project);
    }

    @Test
    @Category(DBCategory.class)
    public void findByIdNull() throws SQLException {
        @NotNull final Project project = projectRepository.findById("testUser", null);
        Assert.assertNull(project);
    }

    @Test
    @Category(DBCategory.class)
    public void findByIdIncorrectUser() throws SQLException {
        @NotNull final Project project = projectRepository.findById("test", this.project.getId());
        Assert.assertNull(project);
    }

    @Test
    @Category(DBCategory.class)
    public void remove() throws SQLException {
        projectRepository.removeById(project.getId());
        Assert.assertNull(projectRepository.findById(project.getId()));
    }

    @Test
    @Category(DBCategory.class)
    public void findByName() {
        @NotNull final Project project = projectRepository.findOneByName("c9779b3f-1171-4d7d-8dea-1138bbfdc9ce", "Project");
        Assert.assertNotNull(project);
    }

    @Test
    @Category(DBCategory.class)
    public void findByNameIncorrect() {
        @NotNull final Project project = projectRepository.findOneByName("c9779b3f-1171-4d7d-8dea-1138bbfdc9ce", "34");
        Assert.assertNull(project);
    }

    @Test
    @Category(DBCategory.class)
    public void findByNameNull() {
        @NotNull final Project project = projectRepository.findOneByName("c9779b3f-1171-4d7d-8dea-1138bbfdc9ce", null);
        Assert.assertNull(project);
    }

    @Test
    @Category(DBCategory.class)
    public void findByNameIncorrectUser() {
        @NotNull final Project project = projectRepository.findOneByName("test", this.project.getName());
        Assert.assertNull(project);
    }

    @Test
    @Category(DBCategory.class)
    public void findByIndex() {
        @NotNull final Project project = projectRepository.findOneByIndex("c9779b3f-1171-4d7d-8dea-1138bbfdc9ce", 1);
        Assert.assertNotNull(project);
    }

    @Test
    @Category(DBCategory.class)
    public void removeById() throws SQLException {
        projectRepository.removeById("c9779b3f-1171-4d7d-8dea-1138bbfdc9ce", project.getId());
        Assert.assertNull(projectRepository.findById(project.getId()));
    }

    @Test
    @Category(DBCategory.class)
    public void removeByIdNull() {
        projectRepository.removeById("c9779b3f-1171-4d7d-8dea-1138bbfdc9ce", null);
    }

    @Test
    @Category(DBCategory.class)
    public void removeByIdIncorrect() {
        projectRepository.removeById("c9779b3f-1171-4d7d-8dea-1138bbfdc9ce", "34");
    }

}

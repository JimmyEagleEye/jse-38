# TASK MANAGER

# DEVELOPER INFO

NAME: Djalal Korkmasov

E-MAIL: dkorkmasov@t1-consulting.ru

# SOFTWARE

* JDK 1.8

* Windows 10

# HARDWARE

* RAM 16GB

* CPU i7

* HDD 250Gb

# BUILD PROGRAM

...
mvn clean install
...

# RUN PROGRAM

java -jar ./jse-33.jar

# SCREENSHOTS

In doc folder


package ru.korkmasov.tsc.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.korkmasov.tsc.api.endpoint.ISessionEndpoint;
import ru.korkmasov.tsc.api.service.ServiceLocator;
import ru.korkmasov.tsc.endpoint.AbstractEndpoint;
import ru.korkmasov.tsc.exception.system.AccessDeniedException;
import ru.korkmasov.tsc.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public final class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    public SessionEndpoint() {
        super(null);
    }

    public SessionEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @WebMethod
    public boolean closeSession(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        try {
            serviceLocator.getSessionService().close(session);
            return true;
        } catch (@NotNull final AccessDeniedException e) {
            return false;
        }
    }

    @Override
    @WebMethod
    public Session openSession(
            @WebParam(name = "login", partName = "login") @NotNull final String login,
            @WebParam(name = "password", partName = "password") @NotNull final String password
    ) {
        return serviceLocator.getSessionService().open(login, password);
    }
}

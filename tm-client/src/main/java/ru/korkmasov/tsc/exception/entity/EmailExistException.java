package ru.korkmasov.tsc.exception.entity;

import ru.korkmasov.tsc.exception.AbstractException;

public class EmailExistException extends AbstractException {

    public EmailExistException(String value) {
        super("Error. Email '" + value + "' already exist.");
    }

}

package ru.korkmasov.tsc.command.task;

import ru.korkmasov.tsc.enumerated.Status;
import ru.korkmasov.tsc.exception.system.IndexIncorrectException;
import ru.korkmasov.tsc.util.TerminalUtil;
import ru.korkmasov.tsc.util.ValidationUtil;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;

public final class TaskByIndexSetStatusCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-set-status-by-index";
    }

    @NotNull
    @Override
    public String description() {
        return "Set task status by index";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[SET TASK STATUS]");
        System.out.println("ENTER INDEX:");
        final int index = TerminalUtil.nextNumber() - 1;
        if (!ValidationUtil.checkIndex(index, serviceLocator.getTaskService().size(userId)))
            throw new IndexIncorrectException();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        serviceLocator.getTaskService().changeTaskStatusByIndex(userId, index, Status.getStatus(TerminalUtil.nextLine()));

    }

}

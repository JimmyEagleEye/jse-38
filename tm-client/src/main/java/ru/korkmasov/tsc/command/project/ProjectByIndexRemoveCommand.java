package ru.korkmasov.tsc.command.project;

import ru.korkmasov.tsc.util.TerminalUtil;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class ProjectByIndexRemoveCommand extends AbstractProjectCommand {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "project-remove-by-index";
    }

    @Override
    public @NotNull String description() {
        return "Remove project by index";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER INDEX:");
        serviceLocator.getProjectService().removeProjectByIndex(userId, TerminalUtil.nextNumber());
    }

}
